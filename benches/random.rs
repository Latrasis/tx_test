use std::collections::HashSet;
use std::fs::File;

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use csv::{Error, StringRecord, Writer};
use rand::{prelude::ThreadRng, random, Rng};
use tx_test::{self, Tx, TxType};

fn new_rng_tx(rng: &mut ThreadRng) -> Tx {
    let tx_type = match rng.gen_range(0..4u8) {
        1 => TxType::Withdrawal,
        2 => TxType::Dispute,
        3 => TxType::Resolve,
        4 => TxType::Chargeback,
        _ => TxType::Deposit,
    };
    Tx {
        tx_type,
        client: rng.gen(),
        id: rng.gen(),
        amount: rng.gen_range(0..1000u32).into(),
    }
}

fn bench_random(c: &mut Criterion) {
    // Generate Rng Input
    let mut rng = rand::thread_rng();

    let mut state = tx_test::ExState::new();
    c.bench_function("ex_state.process_tx", |b| {
        b.iter(|| state.process_tx(black_box(&new_rng_tx(&mut rng))))
    });
}

criterion_group!(benches, bench_random);
criterion_main!(benches);
