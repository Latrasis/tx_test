use std::collections::HashSet;
use std::fs::File;

use csv::{Error, StringRecord};
use tx_test;

#[test]
fn process_sample() {
    let input = File::open("./tests/sample.test.csv").unwrap();
    let expected = File::open("./tests/sample.test.result.csv").unwrap();

    let mut expected_csv = csv::Reader::from_reader(expected);
    let expected_csv: HashSet<tx_test::ClientState> =
        expected_csv.deserialize().filter_map(|v| v.ok()).collect();

    let mut output = Vec::new();
    tx_test::process_csv(input, &mut output, std::io::sink());

    let mut result_csv = csv::Reader::from_reader(output.as_slice());
    let result_csv: HashSet<tx_test::ClientState> =
        result_csv.deserialize().filter_map(|v| v.ok()).collect();

    assert_eq!(expected_csv, result_csv);
}
