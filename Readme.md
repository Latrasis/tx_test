## tx_test
This sample project provides a library and binary for reading a list of transactions data from an csv to outputing a client state result.

_Compiler support: requires rustc 1.59+_

### Example:
```bash
# Install tx_test
cargo install --path .
# Read from file
tx_test input.csv > output.csv
# Note: on Error, Results go to StdErr
tx_test input.csv 2> err.log
# Can also Read from stdin if No Argument Supplied
cat input.csv | tx_test > output.csv
```

#### Sample Input:
```csv
type,client,tx,amount
deposit,1,1,1.0
deposit,2,2,2.0
```

#### Sample Output:
```csv
client,available,held,total,locked
1,1,0,1,false
2,2,0,2,false
```
### Rules and Suggestions:

_Basic Validation:_
1. All Withdrawals and Deposits must have amounts be greater than zero. Otherwise they are ignored.
1. All Disputes, Resolutions and Chargebacks must point to valid Tx/Client Pairs. Otherwise they are ignored.

_On Disputes:_
1. A Dispute can only occur once, and is invalid if the Tx has already been or still is in dispute.
1. A Dispute can still be created even if the Client Account is Frozen. This is justified since disputes after a freeze still need to be trackable and are potentially valid. Thus multiple chargebacks may occur. 

*(In the case that the client account balance already spent the account, this creates a potential issue of the exchange owing to the disputer. In this case, throttling chargebacks or limiting chargeback sums would be stopgap solutions. In the case of a large debt, this is a risk for the exchange to face insolvency. Thus certain policies need to be made to reduce risks, such as trust scoring.)*

_On Resolutions:_
1. Resolutions cannot occur if a transaction has either been resolved or chargedback.
1. Resolutions can still occur even if the client account is frozen.

_On Deposits:_
1. Deposits can still be done, even in the Client Account is Frozen. This is to allow the Client to resolve chargebacks.

_On Withdrawals:_
1. Withdrawals cannot be done if the client does not have available funds.
1. Withdrawals cannot be done if the client account is frozen.

*(In the event of a dispute, it may be pertinent, depending on the Trust Score, to throttle withdrawls in the event of additional disputes.)*

_On Chargebacks:_
1. ChargeBacks cannot occur if a transaction has either been resolved or chargedback. 
1. Chargebacks can still occur even if the client account is frozen. 

### Testing:
Can run both unit and parsing tests via `cargo test`

* Unit Tests are available at: `./src/state.rs`
* Csv Parsing Tests are available at: `./tests/csv.rs`

### Performance:
Uses [Criterion](https://github.com/bheisler/criterion.rs) for basic benchmarking.

* For Benchmarks on `ExState.process_tx` run `cargo bench`.
* Since TxId (`u32`) and ClientId (`u16`) are unordered two HashTables are used for O(1) insertion and deletion.
* We can use [Ahash](https://github.com/tkaitchuck/ahash) for speedup, since this is in-memory.