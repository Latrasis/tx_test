use std::error::Error;
use std::fs::File;
use std::io;

use tx_test;

fn main() -> Result<(), Box<dyn Error>> {
    // Check if File Path is Passed
    let input_fs_path = std::env::args().skip(1).next();

    let input: Box<dyn io::Read> = match input_fs_path {
        // If Help, Provide Manual
        Some(str) if str == "--help" || str == "-help" => {
            println!("Usage: \n\ttx_test <csv_path> | <stdin>");
            return Ok(());
        }

        // If Path, Read from File Path
        Some(path) => Box::new(File::open(path)?),

        // Else, Read from Stdin
        None => Box::new(io::stdin()),
    };

    // Process Csv
    tx_test::process_csv(input, io::stdout(), io::stderr())
}
