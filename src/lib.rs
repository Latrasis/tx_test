mod state;
mod tx;

pub use state::*;
pub use tx::*;

use std::error::Error;
use std::io::{self, BufWriter, Write};

pub fn process_csv<R: io::Read, W: io::Write, S: io::Write>(
    input: R,
    output: W,
    err_output: S,
) -> Result<(), Box<dyn Error>> {
    let mut csv_reader = csv::ReaderBuilder::new()
        .flexible(true)
        .trim(csv::Trim::All)
        .has_headers(true)
        .from_reader(input);

    let mut csv_writer = csv::WriterBuilder::new().from_writer(output);
    let mut err_writer = BufWriter::new(err_output);

    // Initialize State
    let mut ex_state = ExState::new();
    for result in csv_reader.deserialize() {
        let tx: Tx = result?;
        let res = ex_state.process_tx(&tx);

        if let Err(err) = res {
            writeln!(err_writer, "Error: {}", err);
        }
    }

    for client in ex_state.clients.values() {
        csv_writer.serialize(client)?;
    }

    csv_writer.flush()?;
    err_writer.flush()?;

    Ok(())
}
