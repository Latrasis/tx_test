use std::collections::HashMap;

use rust_decimal::prelude::Decimal;
use serde::{Deserialize, Serialize};
use thiserror::Error;
use ahash::{AHasher, RandomState};

use crate::tx::{ClientId, Tx, TxId, TxOrder, TxType};

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Default, Serialize, Deserialize)]
pub struct ClientState {
    pub client: ClientId,
    pub available: Decimal,
    pub held: Decimal,
    pub total: Decimal,
    pub locked: bool,
}

#[derive(Copy, Clone)]
pub struct TxState {
    pub tx: Tx,
    pub is_resolved: bool,
    pub is_in_dispute: bool,
    pub order: TxOrder,
}

pub struct ExState {
    pub clients: HashMap<ClientId, ClientState, RandomState>,
    pub txs: HashMap<TxId, TxState, RandomState>,
    pub order: TxOrder,
}

#[derive(Error, Debug)]
pub enum TxProcessError {
    #[error("Tx#{order}: {tx:?}: Invalid Amount: {}", .tx.amount)]
    InvalidAmount { tx: Tx, order: TxOrder },
    #[error("Tx#{order}: {tx:?}: TxId #{} Already Used", .tx.id)]
    RepeatedTxId { tx: Tx, order: TxOrder },
    #[error("Tx#{order}: {tx:?}: Dispute/Resolution/Chargeback target wrong Client (#{}) or Tx (#{})", .tx.client, .tx.id)]
    InvalidTxIdClientIdPair { tx: Tx, order: TxOrder },
    #[error("Tx#{order}: {tx:?}: Cannot Withdraw {}, Low Funds: {}", .tx.amount, .client.available)]
    LowFunds {
        tx: Tx,
        client: ClientState,
        order: TxOrder,
    },
    #[error("Tx#{order}: {tx:?}: Cannot Withdraw, Client is Frozen")]
    ClientFrozen { tx: Tx, order: TxOrder },
    #[error("Tx#{order}: {tx:?}: Cannot Resolve or ChargeBack if Tx #{} is Not Disputed", .tx.id)]
    NotInDispute { tx: Tx, order: TxOrder },
    #[error("Tx#{order}: {tx:?}: Cannot Despute if Tx #{} is Already in Dispute", .tx.id)]
    AlreadyInDispute { tx: Tx, order: TxOrder },
    #[error("Tx#{order}: {tx:?}: Cannot Resolve, Dispute or Chargeback if Tx #{} is Already Resolved", .tx.id)]
    AlreadyResolved { tx: Tx, order: TxOrder },
    #[error("Tx#{order}: {tx:?}: Other TxType")]
    OtherType { tx: Tx, order: TxOrder },
}

impl ExState {
    pub fn new() -> Self {
        ExState { clients: HashMap::default(), txs: HashMap::default(), order: 0 }
    }

    pub fn process_tx(&mut self, input: &Tx) -> Result<(), TxProcessError> {
        use TxProcessError::*;

        // Increment Order
        self.order += 1;

        match input.tx_type {
            TxType::Deposit => {
                // Check that TxId is new, and has valid amount;
                if self.txs.contains_key(&input.id) {
                    return Err(RepeatedTxId { tx: *input, order: self.order });
                };
                if input.amount.le(&Decimal::ZERO) {
                    return Err(InvalidAmount { tx: *input, order: self.order });
                };

                if let Some(client_state) = self.clients.get_mut(&input.client) {
                    client_state.available += input.amount;
                    client_state.total += input.amount;
                } else {
                    // If Client Is New, Insert New Client with Deposited Balance
                    let amt = input.amount;
                    let new_client = ClientState {
                        client: input.client,
                        available: amt,
                        held: Decimal::ZERO,
                        total: amt,
                        locked: false,
                    };
                    self.clients.insert(input.client, new_client);
                }
                // Save Deposit Tx
                self.txs.insert(
                    input.id,
                    TxState {
                        tx: *input,
                        is_in_dispute: false,
                        is_resolved: false,
                        order: self.order,
                    },
                );
            }
            TxType::Withdrawal => {
                // Check that TxId is new, and has valid amount;
                if self.txs.contains_key(&input.id) {
                    return Err(RepeatedTxId { tx: *input, order: self.order });
                };
                if input.amount.le(&Decimal::ZERO) {
                    return Err(InvalidAmount { tx: *input, order: self.order });
                };

                if let Some(client_state) = self.clients.get_mut(&input.client) {
                    // Check if Client has sufficient available balance, and is not frozen
                    if client_state.available.lt(&input.amount) {
                        return Err(LowFunds {
                            tx: *input,
                            client: *client_state,
                            order: self.order,
                        });
                    }
                    if client_state.locked {
                        return Err(ClientFrozen { tx: *input, order: self.order });
                    }

                    client_state.available -= input.amount;
                    client_state.total -= input.amount;

                    // Save Withdrawal Tx
                    self.txs.insert(
                        input.id,
                        TxState {
                            tx: *input,
                            is_in_dispute: false,
                            is_resolved: false,
                            order: self.order,
                        },
                    );
                }
            }
            TxType::Dispute => {
                // Check that the target Client and Tx exists, ignore if not
                if let (Some(client_state), Some(tx_state)) = (
                    self.clients.get_mut(&input.client),
                    self.txs.get_mut(&input.id),
                ) {
                    // Check Client and Tx Match, Check Tx is Not in Dispute or Resolved
                    if client_state.client != tx_state.tx.client {
                        return Err(InvalidTxIdClientIdPair { tx: *input, order: self.order });
                    }
                    if tx_state.is_in_dispute {
                        return Err(AlreadyInDispute { tx: *input, order: self.order });
                    }
                    if tx_state.is_resolved {
                        return Err(AlreadyResolved { tx: *input, order: self.order });
                    }

                    // Set Dispute Status
                    let tx_amt = tx_state.tx.amount;

                    client_state.available -= tx_amt;
                    client_state.held += tx_amt;
                    tx_state.is_in_dispute = true;
                }
            }
            TxType::Resolve => {
                // Check that the target Client and Tx exists, ignore if not
                if let (Some(client_state), Some(tx_state)) = (
                    self.clients.get_mut(&input.client),
                    self.txs.get_mut(&input.id),
                ) {
                    // Check Client and Tx Match, Check Tx is Not in Dispute
                    if client_state.client != tx_state.tx.client {
                        return Err(InvalidTxIdClientIdPair { tx: *input, order: self.order });
                    }
                    if !tx_state.is_in_dispute {
                        return Err(NotInDispute { tx: *input, order: self.order });
                    }

                    // Set Resolution Status
                    let tx_amt = tx_state.tx.amount;

                    client_state.available += tx_amt;
                    client_state.held -= tx_amt;
                    tx_state.is_in_dispute = false;
                    tx_state.is_resolved = true;
                }
            }
            TxType::Chargeback => {
                // Check that the target Client and Tx exists, ignore if not
                if let (Some(client_state), Some(tx_state)) = (
                    self.clients.get_mut(&input.client),
                    self.txs.get_mut(&input.id),
                ) {
                    // Check Client and Tx Match, Check Tx is in Dispute
                    if client_state.client != tx_state.tx.client {
                        return Err(InvalidTxIdClientIdPair { tx: *input, order: self.order });
                    }
                    if !tx_state.is_in_dispute {
                        return Err(NotInDispute { tx: *input, order: self.order });
                    }

                    // Set Resolution Status
                    let tx_amt = tx_state.tx.amount;

                    client_state.held -= tx_amt;
                    client_state.total -= tx_amt;
                    client_state.locked = true;
                    tx_state.is_in_dispute = false;
                    tx_state.is_resolved = true;
                }
            }
            _ => return Err(OtherType { tx: *input, order: self.order }),
        }
        Ok(())
    }

    pub fn get_state(&self) -> Vec<ClientState> {
        self.clients.values().cloned().collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use TxType::*;

    use rust_decimal_macros::dec;

    fn process_txs(input: Vec<Tx>) -> Vec<ClientState> {
        let mut state = ExState::new();
        for tx in &input {
            state.process_tx(tx);
        }
        state.get_state()
    }

    #[test]
    fn should_ignore_any_repeat_tx() {
        let input1 = vec![
            Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(1) },
            Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(1) },
        ];

        let expected1 = ClientState {
            client: 1,
            available: dec!(1),
            held: dec!(0),
            total: dec!(1),
            locked: false,
        };
        let res1 = process_txs(input1).pop().unwrap();
        assert_eq!(res1, expected1);

        let input2 = vec![
            Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(2) },
            Tx { tx_type: Withdrawal, client: 1, id: 2, amount: dec!(1) },
            Tx { tx_type: Withdrawal, client: 1, id: 2, amount: dec!(1) },
        ];

        let expected2 = ClientState {
            client: 1,
            available: dec!(1),
            held: dec!(0),
            total: dec!(1),
            locked: false,
        };

        let res2 = process_txs(input2).pop().unwrap();
        assert_eq!(res2, expected2);
    }

    mod deposit {
        use super::*;
        #[test]
        fn should_deposit() {
            let deposit = Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(1) };

            let expected = ClientState {
                client: 1,
                available: dec!(1),
                held: dec!(0),
                total: dec!(1),
                locked: false,
            };

            let res = process_txs(vec![deposit]).pop().unwrap();
            assert_eq!(res, expected);
        }
    }

    mod withdraw {
        use super::*;
        #[test]
        fn should_withdraw_on_funds() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(2) },
                Tx { tx_type: Withdrawal, client: 1, id: 2, amount: dec!(1) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(1),
                held: dec!(0),
                total: dec!(1),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }

        #[test]
        fn should_not_withdraw_on_low_funds() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(2) },
                Tx { tx_type: Withdrawal, client: 1, id: 2, amount: dec!(3) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(2),
                held: dec!(0),
                total: dec!(2),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }
    }

    mod dispute {
        use super::*;
        #[test]
        fn should_dispute_tx_deposit() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(1) },
                Tx { tx_type: Deposit, client: 1, id: 2, amount: dec!(1) },
                Tx { tx_type: Dispute, client: 1, id: 2, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(1),
                held: dec!(1),
                total: dec!(2),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }

        #[test]
        fn should_not_dispute_tx_deposit_if_already_in_dispute() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(1) },
                Tx { tx_type: Deposit, client: 1, id: 2, amount: dec!(1) },
                Tx { tx_type: Dispute, client: 1, id: 2, amount: dec!(0) },
                Tx { tx_type: Dispute, client: 1, id: 2, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(1),
                held: dec!(1),
                total: dec!(2),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }

        #[test]
        fn should_not_dispute_if_no_tx_deposit() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Deposit, client: 1, id: 2, amount: dec!(1) },
                Tx { tx_type: Dispute, client: 1, id: 3, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(4),
                held: dec!(0),
                total: dec!(4),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }

        #[test]
        fn should_dispute_tx_deposit_with_overdraft() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Withdrawal, client: 1, id: 2, amount: dec!(2) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(-2),
                held: dec!(3),
                total: dec!(1),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }
    }
    mod resolve {
        use super::*;

        #[test]
        fn should_resolve_dispute() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Resolve, client: 1, id: 1, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(3),
                held: dec!(0),
                total: dec!(3),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }

        #[test]
        fn should_not_resolve_if_no_tx() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Resolve, client: 1, id: 3, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(0),
                held: dec!(3),
                total: dec!(3),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }

        #[test]
        fn should_not_resolve_if_no_dispute() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Deposit, client: 1, id: 2, amount: dec!(2) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Resolve, client: 1, id: 2, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(2),
                held: dec!(3),
                total: dec!(5),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }
        #[test]
        fn should_not_dispute_if_already_resolved() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Resolve, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(3),
                held: dec!(0),
                total: dec!(3),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }
    }

    mod chargeback {
        use super::*;
        #[test]
        fn should_chargeback_dispute() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Chargeback, client: 1, id: 1, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(0),
                held: dec!(0),
                total: dec!(0),
                locked: true,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }

        #[test]
        fn should_not_chargeback_if_no_tx() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Chargeback, client: 1, id: 2, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(0),
                held: dec!(3),
                total: dec!(3),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }

        #[test]
        fn should_not_chargeback_if_no_dispute() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Chargeback, client: 1, id: 1, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(3),
                held: dec!(0),
                total: dec!(3),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }

        fn should_not_chargeback_if_no_dispute_on_resolved() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Resolve, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Chargeback, client: 1, id: 1, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(3),
                held: dec!(0),
                total: dec!(3),
                locked: false,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }

        #[test]
        fn should_not_dispute_if_already_chargedback_tx() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Chargeback, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(0),
                held: dec!(0),
                total: dec!(0),
                locked: true,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }
    }

    mod frozen {
        use super::*;
        #[test]
        fn should_dispute_even_if_frozen() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Deposit, client: 1, id: 2, amount: dec!(5) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Chargeback, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Dispute, client: 1, id: 2, amount: dec!(0) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(0),
                held: dec!(5),
                total: dec!(5),
                locked: true,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }

        #[test]
        fn should_deposit_if_frozen() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Chargeback, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Deposit, client: 1, id: 2, amount: dec!(2) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(2),
                held: dec!(0),
                total: dec!(2),
                locked: true,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }

        #[test]
        fn should_not_withdraw_if_frozen() {
            let input = vec![
                Tx { tx_type: Deposit, client: 1, id: 1, amount: dec!(3) },
                Tx { tx_type: Deposit, client: 1, id: 2, amount: dec!(5) },
                Tx { tx_type: Dispute, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Chargeback, client: 1, id: 1, amount: dec!(0) },
                Tx { tx_type: Withdrawal, client: 1, id: 3, amount: dec!(2) },
            ];

            let expected = ClientState {
                client: 1,
                available: dec!(5),
                held: dec!(0),
                total: dec!(5),
                locked: true,
            };

            let res = process_txs(input).pop().unwrap();
            assert_eq!(res, expected);
        }
    }
}
