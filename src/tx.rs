use rust_decimal::prelude::Decimal;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum TxType {
    Deposit,
    Withdrawal,
    Dispute,
    Resolve,
    Chargeback,
    /// Hints that destructuring should not be exhaustive
    #[doc(hidden)]
    #[serde(other)]
    __Other,
}

pub type ClientId = u16;
pub type TxId = u32;
pub type TxOrder = usize;

#[derive(Debug, Clone, Copy, Deserialize, Serialize)]
pub struct Tx {
    #[serde(rename = "type")]
    pub tx_type: TxType,
    pub client: ClientId,
    #[serde(rename = "tx")]
    pub id: TxId,
    #[serde(deserialize_with = "default_if_empty")]
    pub amount: Decimal,
}

// For Reference See: https://github.com/BurntSushi/rust-csv/issues/109
fn default_if_empty<'de, D, T>(de: D) -> Result<T, D::Error>
where
    D: serde::Deserializer<'de>,
    T: serde::Deserialize<'de> + Default,
{
    Option::<T>::deserialize(de).map(|x| x.unwrap_or_else(|| T::default()))
}
